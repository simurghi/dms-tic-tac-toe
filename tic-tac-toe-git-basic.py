# This is a simulation of a Tic-Tac-Toe game with an AI
# The Player will always go first and be "X" by default
# But you can change this bevahior at home to make it harder
from random import randint

move_list = ('TL', 'TM', 'TR', 
             'ML', 'MM', 'MR',
             'BL', 'BM', 'BR')

pos_dict = {
        "TL": " ", "TM": " ", "TR": " ", 
        "ML": " ", "MM": " ", "MR": " ",
        "BL": " ", "BM": " ", "BR": " ",}

def print_board():
    print(f"""
    {pos_dict['TL']} | {pos_dict['TM']} | {pos_dict['TR']} 
    {pos_dict['ML']} | {pos_dict['MM']} | {pos_dict['MR']}
    {pos_dict['BL']} | {pos_dict['BM']} | {pos_dict['BR']}
    """)

def check_winner():
    # Player Victory Positions
    if pos_dict['TL'] == 'X' and pos_dict['TM'] == 'X' and pos_dict['TR'] == 'X':
        return 1
    elif pos_dict['ML'] == 'X' and pos_dict['MM'] == 'X' and pos_dict['MR'] == 'X':
        return 1
    elif pos_dict['BL'] == 'X' and pos_dict['BM'] == 'X' and pos_dict['BR'] == 'X':
        return 1
    elif pos_dict['TL'] == 'X' and pos_dict['ML'] == 'X' and pos_dict['BL'] == 'X':
        return 1
    elif pos_dict['TM'] == 'X' and pos_dict['MM'] == 'X' and pos_dict['BM'] == 'X':
        return 1
    elif pos_dict['TR'] == 'X' and pos_dict['MR'] == 'X' and pos_dict['BR'] == 'X':
        return 1
    elif pos_dict['TL'] == 'X' and pos_dict['MM'] == 'X' and pos_dict['BR'] == 'X':
        return 1
    elif pos_dict['TR'] == 'X' and pos_dict['MM'] == 'X' and pos_dict['BL'] == 'X':
        return 1
    elif pos_dict['TL'] == 'O' and pos_dict['TM'] == 'O' and pos_dict['TR'] == 'O':
        return -1
    elif pos_dict['ML'] == 'O' and pos_dict['MM'] == 'O' and pos_dict['MR'] == 'O':
        return -1
    elif pos_dict['BL'] == 'O' and pos_dict['BM'] == 'O' and pos_dict['BR'] == 'O':
        return -1
    elif pos_dict['TL'] == 'O' and pos_dict['ML'] == 'O' and pos_dict['BR'] == 'O':
        return -1
    elif pos_dict['TM'] == 'O' and pos_dict['MM'] == 'O' and pos_dict['BM'] == 'O':
        return -1
    elif pos_dict['TR'] == 'O' and pos_dict['MR'] == 'O' and pos_dict['BR'] == 'O':
        return -1
    elif pos_dict['TL'] == 'O' and pos_dict['MM'] == 'O' and pos_dict['BR'] == 'O':
        return -1
    elif pos_dict['TR'] == 'O' and pos_dict['MM'] == 'O' and pos_dict['BL'] == 'O':
        return -1
    else:
        return 0

while True: 
    break
